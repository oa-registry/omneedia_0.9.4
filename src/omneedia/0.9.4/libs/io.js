function setToken() {
  var d = new Date().toMySQL().split(" ")[0];
  return Base64.encode(md5(d));
}

App.apply(App, {
  io: {
    socket: {},
    connect: function (o) {
      var me = this;
      this.socket = socketClusterClient.create();
      window.socket = this.socket;
      (async () => {
        for await (let msg of me.socket.listener("message")) {
        }
      })();
      (async () => {
        for await (let msg of me.socket.listener("authenticate")) {
          //alert(JSON.stringify(msg));
        }
      })();

      (async () => {
        let channel = socket.subscribe("dev");
        for await (let data of channel) {
          if (data == "reload") return window.location.reload(true);
        }
      })();
      (async () => {
        for await (let s of me.socket.listener("connect")) {
          if (Settings.DEBUG) {
            console.log(
              "%c*** omneedia is running in development mode",
              "font-weight:bold"
            );
            console.log(JSON.stringify(s, null, 4));
            App.unblur();
            try {
              document.querySelector(".omneedia-overlay").style.display =
                "none";
            } catch (e) {}
          }
        }
      })();
      (async () => {
        for await (let disconnect of me.socket.listener("close")) {
          console.log("** disconnected");
          if (Settings.DEBUG) {
            App.blur();
            setTimeout(function () {
              document.location.reload(true);
            }, 2000);
            try {
              document.querySelector(".omneedia-overlay").style.display = "";
            } catch (e) {}
          }
        }
      })();
    },
    subscribe: function (str, options, cb) {
      var me = this;
      var sc = this.socket.subscribe(str, options);
      this.socket.emit(str);
      sc.on("subscribe", function (r) {
        me.socket.emit(str);
        if (cb) cb(null, r);
      });
      sc.on("subscribeFail", function (err) {
        if (cb) cb(err);
      });
      return sc;
    },
    send: function (event, str, opt) {
      return this.socket.emit(event, str, cb);
    },
    on: function (s, cb) {
      return this.socket.on(s, cb);
    },
    off: function (s, cb) {
      return this.socket.off(s, cb);
    },
    publish: function (channelName, data, callback) {
      return this.socket.publish(channelName, data, callback);
    },
    unsubscribe: function (channelName) {
      return this.socket.unsubscribe(channelName);
    },
    watch: function (channelName, handler) {
      return this.socket.watch(channelName, handler);
    },
    unwatch: function (channelName, handler) {
      return this.socket.unwatch(channelName, handler);
    },
    watchers: function (channelName) {
      return this.socket.watchers(channelName);
    },
    subscriptions: function () {
      return this.socket.subscriptions(true);
    },
  },
});

App.io.connect();
